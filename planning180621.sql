-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 04:15 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `planning180621`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-17 12:02:56', '2021-06-17 12:02:56', '2021-06-17 12:03:01', 0, 'Abbott', '950ae9e84b68780a42f0b6152e843c19'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-17 12:13:15', '2021-06-17 12:13:15', '2021-06-17 12:13:27', 0, 'Abbott', '2e88e71c4b58e031b1718d949e1234ae'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-17 14:02:51', '2021-06-17 14:02:51', '2021-06-17 14:03:15', 0, 'Abbott', '97fbe4b97042731ef7c7b8fad1ba6fc5'),
(4, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-17 14:07:42', '2021-06-17 14:07:42', '2021-06-17 14:07:59', 0, 'Abbott', '20454808e0a5ed6947b1a8e6f6136146'),
(5, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-17 14:08:13', '2021-06-17 14:08:13', '2021-06-17 14:08:26', 0, 'Abbott', 'ba1ac4fdbb2ca30f67180c488b31cab3'),
(6, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut', 'Abbott', NULL, NULL, '2021-06-17 20:10:41', '2021-06-17 20:10:41', '2021-06-17 21:40:41', 0, 'Abbott', '02952391ef22786ca08e62471e6816b8'),
(7, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-18 15:22:15', '2021-06-18 15:22:15', '2021-06-18 15:22:20', 0, 'Abbott', 'e7f60b2a6daef2162b3c1d41adcbc68a'),
(8, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita institute of medical sciences', NULL, NULL, '2021-06-18 15:30:31', '2021-06-18 15:30:31', '2021-06-18 17:00:31', 0, 'Abbott', 'd51396e3f952608899f8906d7966872b'),
(9, 'Sandra Shammy', 'sandrashammy83609@gmail.com', 'Ernakulam', 'Amrita hospital,Edapilly', NULL, NULL, '2021-06-18 19:00:57', '2021-06-18 19:00:57', '2021-06-18 19:01:37', 0, 'Abbott', '330d1ddd7f63243dc469505187f089ac'),
(10, 'SARATH SK', 'sarathsksjm@gmail.com', 'THIRUVANANTHAPURAM', 'Abbott', NULL, NULL, '2021-06-18 19:01:50', '2021-06-18 19:01:50', '2021-06-18 20:31:50', 0, 'Abbott', '523641a87636f0c7e7e64a21f26afd8a'),
(11, 'Udit ', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-18 19:02:08', '2021-06-18 19:02:08', '2021-06-18 20:32:08', 0, 'Abbott', '69fabf5a4494ec8f72abb3d668136d38'),
(12, 'Kavya M S', 'kavyamekkttu123@gmail.com', 'Ernakulam', 'AIMS', NULL, NULL, '2021-06-18 19:02:26', '2021-06-18 19:02:26', '2021-06-18 20:32:26', 0, 'Abbott', 'c500296b5033a0f53358a53b8ea44d4a'),
(13, 'Sandra Shammy', 'sandrashammy83609@gmail.com', 'Ernakulam', 'Amrita hospital,Edapilly', NULL, NULL, '2021-06-18 19:03:02', '2021-06-18 19:03:02', '2021-06-18 19:10:06', 0, 'Abbott', 'c9fa6a2f69e0f14bfd3832b8453bfd48'),
(14, 'Praveen SV', 'praveencardio@gmail.com', 'Trivandrum', 'KIMS', NULL, NULL, '2021-06-18 19:03:27', '2021-06-18 19:03:27', '2021-06-18 20:33:27', 0, 'Abbott', '415155747af95e54f64423fd1da897a7'),
(15, 'Ajith Jacob', 'ajith.jacob@abbott.com', 'Cochin', 'AV', NULL, NULL, '2021-06-18 19:03:27', '2021-06-18 19:03:27', '2021-06-18 20:33:27', 0, 'Abbott', '35a1a923c0aa19f880d65497a7a8125c'),
(16, 'Rakhi Vijayam', 'rakhivjn5@gmail.com', 'Trivandrum', 'AIMS', NULL, NULL, '2021-06-18 19:04:01', '2021-06-18 19:04:01', '2021-06-18 19:04:11', 0, 'Abbott', 'aa6e16fcc240dff9435a8d28956fb1bb'),
(17, 'Nayana', 'nayanamv@gmail.com', 'Trivandrum', 'Ananthapuri Hospital', NULL, NULL, '2021-06-18 19:04:39', '2021-06-18 19:04:39', '2021-06-18 20:34:39', 0, 'Abbott', '449bea2a0b2f4b931f3e083b873edc5d'),
(18, 'Kavya M S', 'kavyamekkttu123@gmail.com', 'Ernakulam', 'AIMS', NULL, NULL, '2021-06-18 19:05:29', '2021-06-18 19:05:29', '2021-06-18 20:35:29', 0, 'Abbott', '535926680a55e8198e7db664fd6861c7'),
(19, 'Rakhi Vijayan', 'rakhivjn5@gmail.com', 'Trivandrum', 'AIMS', NULL, NULL, '2021-06-18 19:05:30', '2021-06-18 19:05:30', '2021-06-18 19:05:54', 0, 'Abbott', '88c1f9151757e41bf05ee803ec49c48c'),
(20, 'Gopika panicker', 'gopikapanicker2000@gamil.com', 'Kochi', 'Amrita kochi', NULL, NULL, '2021-06-18 19:05:36', '2021-06-18 19:05:36', '2021-06-18 20:35:36', 0, 'Abbott', '5ba881e138757bfa0919a254a55d920f'),
(21, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-06-18 19:05:46', '2021-06-18 19:05:46', '2021-06-18 20:35:46', 0, 'Abbott', '6647381bd07799f1fa59c8ee792246be'),
(22, 'Rakhi Vijayan', 'rakhivjn5@gmail.com', 'Trivandrum', 'AIMS', NULL, NULL, '2021-06-18 19:06:16', '2021-06-18 19:06:16', '2021-06-18 20:36:16', 0, 'Abbott', '2a1842f6c46493f06e97d1b65a387efa'),
(23, 'Revathy.m', 'revathyresmimurali@gmail.com', 'Ernakulam', 'Amrita institute of medical science', NULL, NULL, '2021-06-18 19:06:38', '2021-06-18 19:06:38', '2021-06-18 19:09:51', 0, 'Abbott', '90b4e1113b6529c43068f8b13824c580'),
(24, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-18 19:07:26', '2021-06-18 19:07:26', '2021-06-18 19:07:34', 0, 'Abbott', 'a6f5e6450344bcabe7ee064084bb3a8d'),
(25, 'Neeraja v', 'Neerajav331@gmail.com', 'Kochi', 'Amrita institute of medical science ', NULL, NULL, '2021-06-18 19:07:35', '2021-06-18 19:07:35', '2021-06-18 19:09:46', 0, 'Abbott', '53de27c67cef613c5cd041221aedbfc8'),
(26, 'Gopika panicker', 'gopikapanicker2000@gamil.com', 'Kochi', 'Amrita kochi', NULL, NULL, '2021-06-18 19:08:26', '2021-06-18 19:08:26', '2021-06-18 20:38:26', 0, 'Abbott', '058a2bb8a28b069ae72183a7196da9de'),
(27, 'Haripriya K', 'haripriyak2k01@gmail.com', 'Kochi', 'Amrita', NULL, NULL, '2021-06-18 19:08:27', '2021-06-18 19:08:27', '2021-06-18 19:10:09', 0, 'Abbott', '29ff46923dd173c6de1901b9bf3b2e78'),
(28, 'Nishanth', 'nish@gmail.com', 'Bang', 'bng', NULL, NULL, '2021-06-18 19:08:42', '2021-06-18 19:08:42', '2021-06-18 19:08:53', 0, 'Abbott', 'e228ac7fb661ad5bc2007f5715e94389'),
(29, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'bangalore', NULL, NULL, '2021-06-18 19:09:15', '2021-06-18 19:09:15', '2021-06-18 19:09:28', 0, 'Abbott', '005f10cd2096a56a41b624931410bbe2'),
(30, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Trivandrum', 'Abbott', NULL, NULL, '2021-06-18 19:09:16', '2021-06-18 19:09:16', '2021-06-18 20:39:16', 0, 'Abbott', '7c88407f0fae59949759588c7feac8d5'),
(31, 'ShivaMuralee.S', 'shivamuralee25@gmail.com', 'Trivandrum ', 'AIMS', NULL, NULL, '2021-06-18 19:10:01', '2021-06-18 19:10:01', '2021-06-18 20:40:01', 0, 'Abbott', 'fdf9824a163decd003c912e3f07e5093'),
(32, 'Jibin ', 'jibingeorge@gmail.com', 'Cochin ', 'Lisie', NULL, NULL, '2021-06-18 19:10:41', '2021-06-18 19:10:41', '2021-06-18 20:40:41', 0, 'Abbott', 'acd7176b169ed08b11d7060858a2b123'),
(33, 'shyam sasidharan', 'shyamsasi02@gmail.com', 'Kottarakara', 'Vijaya Heart Centre', NULL, NULL, '2021-06-18 19:10:52', '2021-06-18 19:10:52', '2021-06-18 20:40:52', 0, 'Abbott', '28c649b40b2efb9937dbcc1d22855f6f'),
(34, 'Rakhi Vijayan', 'rakhivjn5@gmail.com', 'Trivandrum', 'AIMS', NULL, NULL, '2021-06-18 19:10:59', '2021-06-18 19:10:59', '2021-06-18 20:40:59', 0, 'Abbott', '5bdae7e6b909a5a10eaf3e110e49761d'),
(35, 'Sandra Shammy', 'sandrashammy83609@gmail.com', 'Ernakulam', 'Amrita hospital,Edapilly', NULL, NULL, '2021-06-18 19:11:01', '2021-06-18 19:11:01', '2021-06-18 19:11:29', 0, 'Abbott', '23be124859febe07f3466636bb475fc0'),
(36, 'Apoorva biju', 'apoorvabijut@gmail.com', 'Kochi', 'Aims', NULL, NULL, '2021-06-18 19:11:57', '2021-06-18 19:11:57', '2021-06-18 20:41:57', 0, 'Abbott', 'b4b2f12d9b91c835bde07269dcb24eb9'),
(37, 'Sujesh V S', 'jibintthomas@gmail.com', 'Kochi ', 'Lisie', NULL, NULL, '2021-06-18 19:12:10', '2021-06-18 19:12:10', '2021-06-18 20:42:10', 0, 'Abbott', 'd77135b4d89caf074c8b20be29ccea2a'),
(38, 'Navajith V T', 'navajith.thankam@abbott.com', 'Calicut', 'AV', NULL, NULL, '2021-06-18 19:12:13', '2021-06-18 19:12:13', '2021-06-18 20:42:13', 0, 'Abbott', '314a8b6e7dd671c19ef30556ad092aad'),
(39, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-18 19:13:32', '2021-06-18 19:13:32', '2021-06-18 20:43:32', 0, 'Abbott', 'a0c1e79f9c92efb377df13727d414d92'),
(40, 'Anup Kumar', 'anupkmr@gmail.com', 'Trivandrum', 'SUT', NULL, NULL, '2021-06-18 19:14:50', '2021-06-18 19:14:50', '2021-06-18 20:44:50', 0, 'Abbott', 'e999327f27ef9cdc8feae3cf64a10252'),
(41, 'H Harsha Nair', 'nairharsha26@gmail.com', 'Ernakulam', 'Amrita Hospital', NULL, NULL, '2021-06-18 19:18:15', '2021-06-18 19:18:15', '2021-06-18 20:48:15', 0, 'Abbott', '3a6bc476a559d091e9f8b1351570436a'),
(42, 'Muralikrishna', 'muralikrishnaindia@gmail.com', 'Cochi', 'Amrita hospital', NULL, NULL, '2021-06-18 19:19:27', '2021-06-18 19:19:27', '2021-06-18 20:49:27', 0, 'Abbott', 'd25ae0b3c511516135b967a33dbc8d40'),
(43, 'Arun Krishnan', 'ak6520938@gmail.com', 'Kochi', 'AIMS', NULL, NULL, '2021-06-18 19:19:38', '2021-06-18 19:19:38', '2021-06-18 20:49:38', 0, 'Abbott', '06f4e40949a1b104a4908ddfbdb9d8fd'),
(44, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-06-18 19:19:47', '2021-06-18 19:19:47', '2021-06-18 20:49:47', 0, 'Abbott', 'fc2bc61c2fbf098c6abae316fc48c2ea'),
(45, 'Shihab Shibu', 'shihablabbas@gmail.com', 'Kochi', 'Amrita ', NULL, NULL, '2021-06-18 19:20:21', '2021-06-18 19:20:21', '2021-06-18 20:50:21', 0, 'Abbott', '16e43cc53513fcebed5d860a5ab4114f'),
(46, 'Pragathy', 'pragathykvelayudhan@gmail.com', 'Ernakulam', 'Amrita', NULL, NULL, '2021-06-18 19:21:10', '2021-06-18 19:21:10', '2021-06-18 20:51:10', 0, 'Abbott', 'c0e2fedbeecd9e8686a9b4978ade3fd0'),
(47, 'Aiswarya S Ajay', 'aiswaryasankar4322@gmail.com', 'Kochi', 'Amrita', NULL, NULL, '2021-06-18 19:21:41', '2021-06-18 19:21:41', '2021-06-18 20:51:41', 0, 'Abbott', '2101466f4d2658c0b0fb2c30d5fa88b8'),
(48, 'Rajitha', 'rajithaov6@gmail.com', 'Trivandrum', 'kims', NULL, NULL, '2021-06-18 19:22:26', '2021-06-18 19:22:26', '2021-06-18 20:52:26', 0, 'Abbott', 'f76d4ddd4a060fd78edc9a66c8be27cf'),
(49, 'Azeemsha', 'Azeemsha24@gmail.com', 'Trivandrum', 'MCH', NULL, NULL, '2021-06-18 19:23:42', '2021-06-18 19:23:42', '2021-06-18 20:53:42', 0, 'Abbott', '87000178048e1ae7a6dc0b700ef7a69d'),
(50, 'Anju k g', 'anjukg13@gmail.com', 'Trivandrum', 'GH', NULL, NULL, '2021-06-18 19:24:15', '2021-06-18 19:24:15', '2021-06-18 20:54:15', 0, 'Abbott', '5f99cb87d4e6a0243bca4ae5b799947f'),
(51, 'Dr. Vinod', 'vinod.manikandan@gmail.com', 'Kollam', 'Holycross', NULL, NULL, '2021-06-18 19:25:22', '2021-06-18 19:25:22', '2021-06-18 20:55:22', 0, 'Abbott', 'afe3b0ad24b9e9b9f98b8e26d2d61884'),
(52, 'Sujith Kumar', 'drsujithkumar1976@rediffmail.com', 'Ernakulam', 'Lourdes', NULL, NULL, '2021-06-18 19:26:36', '2021-06-18 19:26:36', '2021-06-18 20:56:36', 0, 'Abbott', '37bbfd8ca5ec929df4c4b53a3a28df0d'),
(53, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-06-18 19:26:41', '2021-06-18 19:26:41', '2021-06-18 20:56:41', 0, 'Abbott', '136c3d51b86081bd063fb1f77959f79d'),
(54, 'Lijo ', 'lijo.k@abbott.com', 'Cochin ', 'Abbott ', NULL, NULL, '2021-06-18 19:26:43', '2021-06-18 19:26:43', '2021-06-18 20:56:43', 0, 'Abbott', 'da8a68ed23a7afc33513f085564c2ade'),
(55, 'Elias', 'eliaskingmaker123@gmail.com', 'Edapply', 'Amrita', NULL, NULL, '2021-06-18 19:27:24', '2021-06-18 19:27:24', '2021-06-18 20:57:24', 0, 'Abbott', '343273050ffd34e9d6d99b6d00ce1c52'),
(56, 'Gopi krishna j ', 'gopi2k98@gmail.com', 'Ernakulam ', 'AIMS KOCHI', NULL, NULL, '2021-06-18 19:27:43', '2021-06-18 19:27:43', '2021-06-18 20:57:43', 0, 'Abbott', 'c11046b7c7d62b1fe7fd961437810634'),
(57, 'Nithin Krishna', 'nithinkrishna17@gmail.com', 'Kochi', 'AIMS Kochi', NULL, NULL, '2021-06-18 19:28:12', '2021-06-18 19:28:12', '2021-06-18 20:58:12', 0, 'Abbott', '45feed2287ab3c2ac7bfff34a3c83158'),
(58, 'Sandra Shammy', 'sandrashammy83609@gmail.com', 'Ernakulam', 'Amrita hospital,Edapilly', NULL, NULL, '2021-06-18 19:28:24', '2021-06-18 19:28:24', '2021-06-18 19:29:49', 0, 'Abbott', 'e49b632253f4f8dabaf3a402306954ed'),
(59, 'H Harsha Nair', 'nairharsha26@gmail.com', 'Ernakulam', 'Amrita Hospitals', NULL, NULL, '2021-06-18 19:28:59', '2021-06-18 19:28:59', '2021-06-18 20:58:59', 0, 'Abbott', 'b2b46321a6d7dc9365fd9bbc99942aa3'),
(60, 'Sujal Shetty', 'sujal368@gmail.com', 'Mangaluru', 'Yenepoya medical college', NULL, NULL, '2021-06-18 19:31:44', '2021-06-18 19:31:44', '2021-06-18 21:01:44', 0, 'Abbott', '80f821847d69b38208b507892312b1f8'),
(61, 'Pradeep', 'cardioprd@yahoo.com', 'Trivandrum', 'Cosmo', NULL, NULL, '2021-06-18 19:33:25', '2021-06-18 19:33:25', '2021-06-18 21:03:25', 0, 'Abbott', '9f0607ccaf37a108c3cc23d34895a4c4'),
(62, 'SNEHA SHAJI ', 'snehashaji1998@gmail.com', 'CHERTHALA', 'AIMS KOCHI', NULL, NULL, '2021-06-18 19:34:41', '2021-06-18 19:34:41', '2021-06-18 21:04:41', 0, 'Abbott', '1b6a6ac154f024dc285a9276172ef6a4'),
(63, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-06-18 19:36:39', '2021-06-18 19:36:39', '2021-06-18 21:06:39', 0, 'Abbott', 'e0405d5cabf48c5c64d4fea431838f8b'),
(64, 'Sujal Shetty', 'sujal368@gmail.com', 'Mangaluru', 'Yenepoya medical college', NULL, NULL, '2021-06-18 19:37:38', '2021-06-18 19:37:38', '2021-06-18 21:07:38', 0, 'Abbott', 'e99adb4c893e8678f4206f360c55f057'),
(65, 'Karthika.p.nair', 'minimpnair@gmail.com', 'Kochi', 'Amritha institute of medical science', NULL, NULL, '2021-06-18 19:40:27', '2021-06-18 19:40:27', '2021-06-18 21:10:27', 0, 'Abbott', '5c594b42a9017800bc3784c3a6cc9957'),
(66, 'H Harsha Nair', 'nairharsha26@gmail.com', 'Ernakulam', 'Amrita Hospital', NULL, NULL, '2021-06-18 19:42:17', '2021-06-18 19:42:17', '2021-06-18 21:12:17', 0, 'Abbott', 'f465a9dcdbfc96cf7f634cb35630e313'),
(67, 'Ramesh', 'ramesh.mnair@abbott.com', 'Calicut ', 'NA', NULL, NULL, '2021-06-18 19:44:52', '2021-06-18 19:44:52', '2021-06-18 21:14:52', 0, 'Abbott', '39ce3a36c99ac938679182d39ff6159f'),
(68, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita institute of medical sciences', NULL, NULL, '2021-06-18 19:52:14', '2021-06-18 19:52:14', '2021-06-18 21:22:14', 0, 'Abbott', 'aa7de8d842e0f06a98334b5cf3cd0a5a'),
(69, 'Sandra Shammy', 'sandrashammy83609@gmail.com', 'Ernakulam', 'Amrita hospital,Edapilly', NULL, NULL, '2021-06-18 19:55:42', '2021-06-18 19:55:42', '2021-06-18 19:57:57', 0, 'Abbott', '6e7de01f4996c2cc2bd7551e67579b73'),
(70, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita institute of medical sciences', NULL, NULL, '2021-06-18 19:56:42', '2021-06-18 19:56:42', '2021-06-18 21:26:42', 0, 'Abbott', '79a6bea89b469630bb0341bc3e0ce91e'),
(71, 'Aiswarya S Ajay', 'aiswaryasankar4322@gmail.com', 'Kochi', 'Amrita', NULL, NULL, '2021-06-18 19:57:04', '2021-06-18 19:57:04', '2021-06-18 21:27:04', 0, 'Abbott', 'd70a7b44e33e3c9a052228f3be0a7fa8'),
(72, 'Sandra Shammy', 'sandrashammy83609@gmail.com', 'Ernakulam', 'Amrita hospital,Edapilly', NULL, NULL, '2021-06-18 19:59:05', '2021-06-18 19:59:05', '2021-06-18 20:19:16', 0, 'Abbott', 'a1b3302c3e7ff5eb8e8f85c400ef96cf'),
(73, 'Jomon', 'jomongregory@gmail.com', 'Ktym', 'Caritas', NULL, NULL, '2021-06-18 20:01:14', '2021-06-18 20:01:14', '2021-06-18 21:31:14', 0, 'Abbott', 'e578121d92b16560f996fd6673b739a1'),
(74, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut', 'Abbott', NULL, NULL, '2021-06-18 20:01:16', '2021-06-18 20:01:16', '2021-06-18 21:31:16', 0, 'Abbott', 'd6dd8903ebdab222b4eea89cddf22964'),
(75, 'Kavya M S', 'kavyamekkttu123@gmail.com', 'Ernakulam', 'AIMS', NULL, NULL, '2021-06-18 20:05:20', '2021-06-18 20:05:20', '2021-06-18 21:35:20', 0, 'Abbott', 'bae63c000c79cb0ead7f8e44376fb66d'),
(76, 'Shiva Muralee ', 'shivamuralee25@gmail.com', 'Trivandrum ', 'AIMS', NULL, NULL, '2021-06-18 20:10:09', '2021-06-18 20:10:09', '2021-06-18 21:40:09', 0, 'Abbott', 'cb689e0ba9c8e8d16cf6da83b9b847a0'),
(77, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita institute of medical sciences', NULL, NULL, '2021-06-18 20:13:03', '2021-06-18 20:13:03', '2021-06-18 21:43:03', 0, 'Abbott', 'b904f60b301402115211c32039b90e1f'),
(78, 'H', 'acbfhy@gmail.com', 'Kochi', 'Mth', NULL, NULL, '2021-06-18 20:15:39', '2021-06-18 20:15:39', '2021-06-18 21:01:23', 0, 'Abbott', '5edc07f2a7098e28fde23ab261476045'),
(79, 'Arunkumar', 'drakgindia@gmail.com', 'cochin', 'GMC ernakulam', NULL, NULL, '2021-06-18 20:20:11', '2021-06-18 20:20:11', '2021-06-18 20:21:07', 0, 'Abbott', '9dba61af0491c50660750608739a19bf'),
(80, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita institute of medical sciences', NULL, NULL, '2021-06-18 20:27:09', '2021-06-18 20:27:09', '2021-06-18 21:57:09', 0, 'Abbott', '41779208e0a85ecb262ec30896fe75a4'),
(81, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Trivandrum', 'Abbott', NULL, NULL, '2021-06-18 20:28:36', '2021-06-18 20:28:36', '2021-06-18 21:58:36', 0, 'Abbott', 'ce5aa12c299c6a30ebc0801776540e54'),
(82, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita institute of medical sciences', NULL, NULL, '2021-06-18 20:30:30', '2021-06-18 20:30:30', '2021-06-18 22:00:30', 0, 'Abbott', '4aa2710a82f836bb2ae463ff38b38bc2'),
(83, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita institute of medical sciences', NULL, NULL, '2021-06-18 20:37:20', '2021-06-18 20:37:20', '2021-06-18 22:07:20', 0, 'Abbott', 'a18f6c0f040e7a3a0ad0b18ac5cd80f6'),
(84, 'Shivamuralee ', 'shivamuralee25@gmail.com', 'Trivandrum ', 'AIMS', NULL, NULL, '2021-06-18 20:39:14', '2021-06-18 20:39:14', '2021-06-18 21:07:40', 0, 'Abbott', 'ab2a0c73c861658ddcaae8dc1d55a95c'),
(85, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-18 20:51:42', '2021-06-18 20:51:42', '2021-06-18 22:21:42', 0, 'Abbott', '724e8b41a9aae77e85364977b1a7800c'),
(86, 'Neeraja v', 'Neerajav331@gmail.com', 'Kochi', 'Amrita institute of medical science ', NULL, NULL, '2021-06-18 20:55:33', '2021-06-18 20:55:33', '2021-06-18 20:57:12', 0, 'Abbott', '5c910dd962e583270fb7ca6b9559301c'),
(87, 'Haripriya K', 'haripriyak2k01@gmail.com', 'Kochi', 'Amrita', NULL, NULL, '2021-06-18 20:55:36', '2021-06-18 20:55:36', '2021-06-18 20:57:15', 0, 'Abbott', '58316faa90dc28eff8022db35e0734e3'),
(88, 'Revathy.m', 'revathyresmimurali@gmail.com', 'Ernakulam', 'Amrita institute of medical science', NULL, NULL, '2021-06-18 20:57:37', '2021-06-18 20:57:37', '2021-06-18 20:57:58', 0, 'Abbott', '7b08e6c17e08a862004f79e0d4e5a068'),
(89, 'Gopika panicker', 'gopikapanicker2000@gamil.com', 'Kochi', 'Amrita kochi', NULL, NULL, '2021-06-18 21:03:52', '2021-06-18 21:03:52', '2021-06-18 22:33:52', 0, 'Abbott', 'ac7e73f6f112bc4310aec3750617b0a8'),
(90, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita institute of medical sciences', NULL, NULL, '2021-06-18 21:04:01', '2021-06-18 21:04:01', '2021-06-18 21:04:18', 0, 'Abbott', '58ef020b5a679eee6f1829ae89cf5845'),
(91, 'Sruthi S Prathap', 'sruthysprathap20@gmail.com', 'Ernakulam', 'Amrita', NULL, NULL, '2021-06-18 21:04:16', '2021-06-18 21:04:16', '2021-06-18 22:34:16', 0, 'Abbott', '3498a26c60f1cf2d64f9e511674d5ab5'),
(92, 'Gloria ', 'gloriapadayattil994@gmail.com', 'Ernakulam ', 'Tech', NULL, NULL, '2021-06-18 21:17:09', '2021-06-18 21:17:09', '2021-06-18 22:47:09', 0, 'Abbott', '933678fec43c5dd3c5ae55df09f38573'),
(93, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-18 21:21:48', '2021-06-18 21:21:48', '2021-06-18 21:21:58', 0, 'Abbott', '3762dcd44a677d19c9b4074d96632af7');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
